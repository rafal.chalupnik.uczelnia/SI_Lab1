﻿using System.Text;

namespace Lab1
{
    public class Result
    {
        private static string IntArrayToString(int[] _array)
        {
            var builder = new StringBuilder();
            foreach (var t in _array)
            {
                builder.Append(t);
                builder.Append(";");
            }
            return builder.ToString();
        }

        public int EvaluationCounter { get; set; }
        public int Score { get; set; }
        public int[] Solution { get; set; }

        public override string ToString()
        {
            var builder = new StringBuilder();
            builder.Append("Solution: ");
            builder.Append(IntArrayToString(Solution));
            builder.Append("\tScore: ");
            builder.Append(Score);
            builder.Append("\tEvaluationCounter: ");
            builder.Append(EvaluationCounter);
            return builder.ToString();
        }
    }
}