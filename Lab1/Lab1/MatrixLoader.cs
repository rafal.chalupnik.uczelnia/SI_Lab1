﻿using System;
using System.IO;
using System.Linq;

namespace Lab1
{
    public class MatrixLoader
    {
        private const int FlowMatrixStartLine = 2;
        private const int SizeLine = 0;

        public int EvaluationCounter { get; private set; }
        public Matrix FlowMatrix { get; private set; }
        public Matrix LocationMatrix { get; private set; }
        public int Size { get; private set; }

        private MatrixLoader() { }
        
        public static MatrixLoader LoadFromFile(string _filePath)
        {
            if (string.IsNullOrEmpty(_filePath))
                throw new ArgumentException("Ścieżka do pliku nie może być pusta!");

            var lines = File.ReadAllLines(_filePath);

            var size = Convert.ToInt32(lines[SizeLine].Split(' ').First(_s => _s.Length != 0));

            var matrixLoader = new MatrixLoader()
            {
                FlowMatrix = new Matrix(size, size),
                LocationMatrix = new Matrix(size, size),
                Size = size
            };

            var flowMatrixEndLine = FlowMatrixStartLine + size - 1;

            for (var i = FlowMatrixStartLine; i <= flowMatrixEndLine; i++)
            {
                var lineValues = lines[i].Split(' ').Where(_s => _s.Length != 0).ToList();
                if (lineValues.Count != size)
                    throw new FileLoadException("Plik jest uszkodzony!");
                for (var j = 0; j < size; j++)
                    matrixLoader.FlowMatrix[i - FlowMatrixStartLine, j] = Convert.ToInt32(lineValues[j]);
            }

            var locationMatrixStartLine = flowMatrixEndLine + 2;
            var locationMatrixEndLine = locationMatrixStartLine + size - 1;

            for (var i = locationMatrixStartLine; i <= locationMatrixEndLine; i++)
            {
                var lineValues = lines[i].Split(' ').Where(_s => _s.Length != 0).ToList();
                if (lineValues.Count != size)
                    throw new FileLoadException("Plik jest uszkodzony!");
                for (var j = 0; j < size; j++)
                    matrixLoader.LocationMatrix[i - locationMatrixStartLine, j] = Convert.ToInt32(lineValues[j]);
            }

            return matrixLoader;
        }

        public int Evaluate(int[] _solution)
        {
            var list = _solution.ToList();
            var cost = 0;

            for (var r = 0; r < Size; r++)
            {
                for (var c = 0; c < Size; c++)
                {
                    cost += FlowMatrix[r, c] * LocationMatrix[list.IndexOf(r), list.IndexOf(c)];
                }
            }

            EvaluationCounter++;
            return cost;
        }
    }
}