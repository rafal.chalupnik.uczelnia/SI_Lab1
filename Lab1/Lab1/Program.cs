﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab1
{
    class Program
    {
        private static void Main(string[] args)
        {
            Console.WriteLine("RandomSearch:");
            for (var i = 0; i < 5; i++)
            {
                var result = RandomSearch(@"C:\Users\Raven\Desktop\had12.dat", 30000);
                Console.WriteLine(result);
            }
            Console.WriteLine();
            Console.WriteLine("GreedySearch:");
            for (var i = 0; i < 5; i++)
            {
                var result = GreedySearch(@"C:\Users\Raven\Desktop\had12.dat");
                Console.WriteLine(result);
            }
            Console.WriteLine();
            Console.WriteLine("HybridSearch:");
            for (var i = 0; i < 5; i++)
            {
                var result = HybridSearch(@"C:\Users\Raven\Desktop\had12.dat", 30000);
                Console.WriteLine(result);
            }
            Console.WriteLine();
            Console.WriteLine("Done!");
            Console.ReadLine();
        }

        private static int[] GenerateRandomSolution(Random _random, int _length)
        {
            var available = new List<int>();
            for (var i = 0; i < _length; i++)
                available.Add(i);
            
            var solution = new int[_length];
            for (var i = 0; i < _length; i++)
            {
                solution[i] = available[_random.Next(available.Count)];
                available.Remove(solution[i]);
            }

            return solution;
        }
        private static Result RandomSearch(string _filePath, int _timeMilis)
        {
            var matrixLoader = MatrixLoader.LoadFromFile(@"C:\Users\Raven\Desktop\had12.dat");

            var watch = System.Diagnostics.Stopwatch.StartNew();

            int[] bestSolution = null;
            var bestScore = int.MaxValue;
            var random = new Random();
            while (watch.ElapsedMilliseconds < _timeMilis)
            {
                var solution = GenerateRandomSolution(random, matrixLoader.Size);
                var score = matrixLoader.Evaluate(solution);
                if (score < bestScore)
                {
                    bestScore = score;
                    bestSolution = solution;
                }
            }

            return new Result()
            {
                Solution = bestSolution,
                Score = bestScore,
                EvaluationCounter = matrixLoader.EvaluationCounter
            };
        }

        private static List<Tuple<int, int>> GetPairs(int _range)
        {
            var list = new List<Tuple<int, int>>();

            for (var i = 0; i < _range; i++)
            {
                for (var j = 0; j < _range; j++)
                {
                    var tuple = i < j
                        ? new Tuple<int, int>(i, j)
                        : new Tuple<int, int>(j, i);

                    if (!list.Contains(tuple))
                        list.Add(tuple);
                }
            }

            return list;
        }

        private static int[] Swap(int[] _array, int _index1, int _index2)
        {
            var copiedArray = new int[_array.Length];
            for (var i = 0; i < _array.Length; i++)
                copiedArray[i] = _array[i];

            var index1Value = copiedArray[_index1];
            copiedArray[_index1] = copiedArray[_index2];
            copiedArray[_index2] = index1Value;
            return copiedArray;
        }
        private static Result GreedySearch(string _filePath)
        {
            var matrixLoader = MatrixLoader.LoadFromFile(@"C:\Users\Raven\Desktop\had12.dat");

            var random = new Random();
            var bestSolution = GenerateRandomSolution(random, matrixLoader.Size); ;
            var bestScore = matrixLoader.Evaluate(bestSolution);

            var atLeastOneSwapped = true;

            while (atLeastOneSwapped)
            {
                atLeastOneSwapped = false;
                var pairs = GetPairs(matrixLoader.Size);

                while (pairs.Count != 0)
                {
                    var pair = pairs.First();
                    pairs.Remove(pair);

                    var solution = Swap(bestSolution, pair.Item1, pair.Item2);
                    var score = matrixLoader.Evaluate(solution);

                    if (score < bestScore)
                    {
                        bestScore = score;
                        bestSolution = solution;
                        atLeastOneSwapped = true;
                    }
                }
            }
            
            return new Result()
            {
                Solution = bestSolution,
                Score = bestScore,
                EvaluationCounter = matrixLoader.EvaluationCounter
            };
        }

        private static Result HybridSearch(string _filePath, int _timeMillis)
        {
            var matrixLoader = MatrixLoader.LoadFromFile(@"C:\Users\Raven\Desktop\had12.dat");
            var bestScore = int.MaxValue;
            int[] bestSolution = null;

            var random = new Random();
            var watch = System.Diagnostics.Stopwatch.StartNew();

            while (watch.ElapsedMilliseconds < _timeMillis)
            {
                var bestIterationSolution = GenerateRandomSolution(random, matrixLoader.Size);
                var bestIterationScore = matrixLoader.Evaluate(bestIterationSolution);
                var atLeastOneSwapped = true;

                while (atLeastOneSwapped)
                {
                    atLeastOneSwapped = false;
                    var pairs = GetPairs(matrixLoader.Size);

                    while (pairs.Count != 0)
                    {
                        var pair = pairs.First();
                        pairs.Remove(pair);

                        var solution = Swap(bestIterationSolution, pair.Item1, pair.Item2);
                        var score = matrixLoader.Evaluate(solution);

                        if (score < bestIterationScore)
                        {
                            bestIterationScore = score;
                            bestIterationSolution = solution;
                            atLeastOneSwapped = true;
                        }
                    }
                }

                if (bestIterationScore < bestScore)
                {
                    bestScore = bestIterationScore;
                    bestSolution = bestIterationSolution;
                }
            }

            return new Result()
            {
                Solution = bestSolution,
                Score = bestScore,
                EvaluationCounter = matrixLoader.EvaluationCounter
            };
        }
    }
}