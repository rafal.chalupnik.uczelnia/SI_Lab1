﻿using System;
using System.Text;

namespace Lab1
{
    public class Matrix
    {
        private int columns_;
        private int[,] content_;
        private int rows_;

        private void CheckCoordinates(int _row, int _column)
        {
            if (_row < 0 || _row >= rows_)
                throw new IndexOutOfRangeException("Wiersz spoza zakresu!");
            if (_column < 0 || _column >= columns_)
                throw new IndexOutOfRangeException("Kolumna spoza zakresu!");
        }

        public Matrix(int _rows, int _columns)
        {
            if (_rows <= 0)
                throw new ArgumentException("Liczba wierszy musi być dodatnia!");
            if (_columns <= 0)
                throw new ArgumentException("Liczba kolumn musi być dodatnia!");

            rows_ = _rows;
            columns_ = _columns;

            content_ = new int[rows_, columns_];
        }

        public int this[int _row, int _column]
        {
            get
            {
                CheckCoordinates(_row, _column);
                return content_[_row, _column];
            }
            set
            {
                CheckCoordinates(_row, _column);
                content_[_row, _column] = value;
            }
        }

        public override string ToString()
        {
            var builder = new StringBuilder();

            for (var row = 0; row < rows_; row++)
            {
                for (var column = 0; column < columns_; column++)
                {
                    builder.Append(content_[row, column].ToString("##"));
                    builder.Append('\t');
                }
                builder.Append('\n');
            }

            return builder.ToString();
        }
    }
}